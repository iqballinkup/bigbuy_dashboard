import 'package:big_buy_dashboard/pages/change_mobile_no.dart';
import 'package:big_buy_dashboard/pages/change_password.dart';
import 'package:big_buy_dashboard/pages/invite.dart';
import 'package:big_buy_dashboard/pages/my_coupon.dart';
import 'package:big_buy_dashboard/pages/my_order.dart';
import 'package:big_buy_dashboard/pages/my_profile.dart';
import 'package:big_buy_dashboard/pages/my_wallet.dart';
import 'package:big_buy_dashboard/pages/update_profile_page.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _selectedDashboardButton = 0;
  double elevatedButtonTextSize = 12.0;
  // bool isRemembered = false;
  // var emailPhoneText = "Email";

  final PageController _controller = PageController();

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    return SafeArea(
      child: Scaffold(
        body: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SingleChildScrollView(
              child: Container(
                width: screenWidth / 3,
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.only(left: 0.5, right: 0.5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        height: 50,
                        width: double.infinity,
                        color: Color(0xff002A56),
                        child: Center(
                          child: Text(
                            'Dashboard',
                            style: GoogleFonts.poppins(
                              textStyle: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: screenWidth / 20,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                      ElevatedButton.icon(
                        onPressed: () {
                          setState(() {
                            _selectedDashboardButton = 0;
                            _controller.jumpToPage(_selectedDashboardButton);
                          });
                        },
                        icon: Align(
                          alignment: Alignment.centerLeft,
                          child: Icon(
                            Icons.person,
                            size: 18,
                            color: _selectedDashboardButton == 0
                                ? Colors.white
                                : Colors.black54,
                          ),
                        ),
                        label: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "My Profile",
                            style: TextStyle(
                                color: _selectedDashboardButton == 0
                                    ? Colors.white
                                    : Colors.black54,
                                fontSize: elevatedButtonTextSize),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: _selectedDashboardButton == 0
                              ? Color(0xff002A56)
                              : Colors.white,
                        ),
                      ),
                      ElevatedButton.icon(
                        onPressed: () {
                          setState(() {
                            _selectedDashboardButton = 1;
                            _controller.jumpToPage(_selectedDashboardButton);
                          });
                        },
                        icon: Align(
                          alignment: Alignment.centerLeft,
                          child: Icon(
                            Icons.person_add,
                            size: 18,
                            color: _selectedDashboardButton == 1
                                ? Colors.white
                                : Colors.black54,
                          ),
                        ),
                        label: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Update Profile",
                            style: TextStyle(
                                color: _selectedDashboardButton == 1
                                    ? Colors.white
                                    : Colors.black54,
                                fontSize: elevatedButtonTextSize),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: _selectedDashboardButton == 1
                              ? Color(0xff002A56)
                              : Colors.white,
                        ),
                      ),
                      ElevatedButton.icon(
                        onPressed: () {
                          setState(() {
                            _selectedDashboardButton = 2;
                            _controller.jumpToPage(_selectedDashboardButton);
                          });
                        },
                        icon: Align(
                          alignment: Alignment.centerLeft,
                          child: Icon(
                            Icons.lock,
                            size: 18,
                            color: _selectedDashboardButton == 2
                                ? Colors.white
                                : Colors.black54,
                          ),
                        ),
                        label: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Change Password",
                            style: TextStyle(
                                color: _selectedDashboardButton == 2
                                    ? Colors.white
                                    : Colors.black54,
                                fontSize: elevatedButtonTextSize),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: _selectedDashboardButton == 2
                              ? Color(0xff002A56)
                              : Colors.white,
                        ),
                      ),
                      ElevatedButton.icon(
                        onPressed: () {
                          setState(() {
                            _selectedDashboardButton = 3;
                            _controller.jumpToPage(_selectedDashboardButton);
                          });
                        },
                        icon: Align(
                          alignment: Alignment.centerLeft,
                          child: Icon(
                            Icons.phone,
                            size: 18,
                            color: _selectedDashboardButton == 3
                                ? Colors.white
                                : Colors.black54,
                          ),
                        ),
                        label: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Change Mobile No.",
                            style: TextStyle(
                                color: _selectedDashboardButton == 3
                                    ? Colors.white
                                    : Colors.black54,
                                fontSize: elevatedButtonTextSize),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: _selectedDashboardButton == 3
                              ? Color(0xff002A56)
                              : Colors.white,
                        ),
                      ),
                      ElevatedButton.icon(
                        onPressed: () {
                          setState(() {
                            _selectedDashboardButton = 4;
                            _controller.jumpToPage(_selectedDashboardButton);
                          });
                        },
                        icon: Align(
                          alignment: Alignment.centerLeft,
                          child: Icon(
                            Icons.production_quantity_limits,
                            size: 18,
                            color: _selectedDashboardButton == 4
                                ? Colors.white
                                : Colors.black54,
                          ),
                        ),
                        label: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "My Order",
                            style: TextStyle(
                                color: _selectedDashboardButton == 4
                                    ? Colors.white
                                    : Colors.black54,
                                fontSize: elevatedButtonTextSize),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: _selectedDashboardButton == 4
                              ? Color(0xff002A56)
                              : Colors.white,
                        ),
                      ),
                      ElevatedButton.icon(
                        onPressed: () {
                          setState(() {
                            _selectedDashboardButton = 5;
                            _controller.jumpToPage(_selectedDashboardButton);
                          });
                        },
                        icon: Align(
                          alignment: Alignment.centerLeft,
                          child: Icon(
                            Icons.wallet,
                            size: 18,
                            color: _selectedDashboardButton == 5
                                ? Colors.white
                                : Colors.black54,
                          ),
                        ),
                        label: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "My Wallet",
                            style: TextStyle(
                                color: _selectedDashboardButton == 5
                                    ? Colors.white
                                    : Colors.black54,
                                fontSize: elevatedButtonTextSize),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: _selectedDashboardButton == 5
                              ? Color(0xff002A56)
                              : Colors.white,
                        ),
                      ),
                      ElevatedButton.icon(
                        onPressed: () {
                          setState(() {
                            _selectedDashboardButton = 6;
                            _controller.jumpToPage(_selectedDashboardButton);
                          });
                        },
                        icon: Align(
                          alignment: Alignment.centerLeft,
                          child: Icon(
                            Icons.list,
                            size: 18,
                            color: _selectedDashboardButton == 6
                                ? Colors.white
                                : Colors.black54,
                          ),
                        ),
                        label: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "My Coupon List",
                            style: TextStyle(
                                color: _selectedDashboardButton == 6
                                    ? Colors.white
                                    : Colors.black54,
                                fontSize: elevatedButtonTextSize),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: _selectedDashboardButton == 6
                              ? Color(0xff002A56)
                              : Colors.white,
                        ),
                      ),
                      ElevatedButton.icon(
                        onPressed: () {
                          setState(() {
                            _selectedDashboardButton = 7;
                            _controller.jumpToPage(_selectedDashboardButton);
                          });
                        },
                        icon: Align(
                          alignment: Alignment.centerLeft,
                          child: Icon(
                            Icons.send,
                            size: 18,
                            color: _selectedDashboardButton == 7
                                ? Colors.white
                                : Colors.black54,
                          ),
                        ),
                        label: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Invite",
                            style: TextStyle(
                                color: _selectedDashboardButton == 7
                                    ? Colors.white
                                    : Colors.black54,
                                fontSize: elevatedButtonTextSize),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: _selectedDashboardButton == 7
                              ? Color(0xff002A56)
                              : Colors.white,
                        ),
                      ),
                      ElevatedButton.icon(
                        onPressed: () {
                          setState(() {
                            _selectedDashboardButton = 8;
                          });
                        },
                        icon: Align(
                          alignment: Alignment.centerLeft,
                          child: Icon(
                            Icons.logout,
                            size: 18,
                            color: _selectedDashboardButton == 8
                                ? Colors.white
                                : Colors.black54,
                          ),
                        ),
                        label: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            "Logout",
                            style: TextStyle(
                                color: _selectedDashboardButton == 8
                                    ? Colors.white
                                    : Colors.black54,
                                fontSize: elevatedButtonTextSize),
                          ),
                        ),
                        style: ElevatedButton.styleFrom(
                          backgroundColor: _selectedDashboardButton == 8
                              ? Color(0xff002A56)
                              : Colors.white,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Expanded(
              child: PageView(
                physics: const NeverScrollableScrollPhysics(),
                controller: _controller,
                children: <Widget>[
                  //Page0
                  MyProfile(),
                  //Page1
                  UpdateProfile(),
                  //Page2
                  ChangePassword(),
                  //Page3
                  ChangeMobile(),
                  //Page4
                  MyOrder(),
                  //Page5
                  MyWallet(),
                  //Page6
                  MyCoupon(),
                  //Page7
                  Invite(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
