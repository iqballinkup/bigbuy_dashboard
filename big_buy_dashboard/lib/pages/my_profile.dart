import 'package:big_buy_dashboard/const/const.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyProfile extends StatefulWidget {
  const MyProfile({super.key});

  @override
  State<MyProfile> createState() => _MyProfileState();
}

class _MyProfileState extends State<MyProfile> {
  TextEditingController couponController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var screenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;

    double textFontSize = 16.0;

    return Scaffold(
      backgroundColor: scaffoldColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 30,
                width: double.infinity,
                color: Color(0xff002A56),
                child: Center(
                  child: Text(
                    'Customer Information',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: textFontSize,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: SizedBox(
                  height: screenWidth / 2,
                  width: screenWidth / 2,
                  child: Image.network(
                      "https://bigbuy.com.bd/uploads/customers/37a25e3509d2f33cb57fec67d1f0ccd0.jpg"),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Table(
                    border: TableBorder.all(color: Colors.black54, width: 1.5),
                    columnWidths: {
                      0: FlexColumnWidth(3),
                      1: FlexColumnWidth(5),
                    },
                    children: [
                      TableRow(children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            "Id",
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: textFontSize,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            "4965290897",
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: textFontSize,
                            ),
                          ),
                        ),
                      ]),
                      TableRow(children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            "Name",
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: textFontSize,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            "uzzal",
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: textFontSize,
                            ),
                          ),
                        ),
                      ]),
                      TableRow(children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            "Phone",
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: textFontSize,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            "01518657125",
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: textFontSize,
                            ),
                          ),
                        ),
                      ]),
                      TableRow(children: [
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            "Address",
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: textFontSize,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(5.0),
                          child: Text(
                            "Sadhukhali",
                            style: TextStyle(
                              color: Colors.black54,
                              fontSize: textFontSize,
                            ),
                          ),
                        ),
                      ]),
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 20.0,
              ),
              Container(
                height: 30,
                width: double.infinity,
                color: Color(0xff002A56),
                child: Center(
                  child: Text(
                    'Apply Coupon',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: textFontSize,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: SizedBox(
                      height: 40,
                      child: TextFormField(
                        controller: couponController,
                        decoration: InputDecoration(
                          filled: true,
                          contentPadding: EdgeInsets.only(
                              left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                          fillColor: Colors.white,
                          border: OutlineInputBorder(),
                          hintText: "Coupon Code",
                          hintStyle: TextStyle(fontSize: 14),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return null;
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  Container(
                      height: 40,
                      child: ElevatedButton(
                          onPressed: () {}, child: Text("Apply"))),
                ],
              ),
              SizedBox(
                height: 10.0,
              ),
              SizedBox(
                height: 120,
                width: double.infinity,
                child: Card(
                  elevation: 8,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text("Wallet Balance",
                              style: TextStyle(
                                  color: Color(0xff0D83CB),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "৳",
                              style: TextStyle(
                                  color: Color(0xff13D18B), fontSize: 24),
                            ),
                            Text(
                              "0",
                              style: TextStyle(
                                  color: Color(0xff13D18B), fontSize: 24),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(
                              "Hold Balance: ",
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 15),
                            ),
                            Text(
                              "0",
                              style: TextStyle(
                                  color: Colors.black54, fontSize: 15),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              SizedBox(
                height: 120,
                width: double.infinity,
                child: Card(
                  elevation: 8,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text("Orders",
                              style: TextStyle(
                                  color: Color(0xff0D83CB),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(
                              Icons.production_quantity_limits,
                              size: 25.0,
                              color: Color(0xff13D18B),
                            ),
                            Text(
                              "Total: 0",
                              style: TextStyle(
                                  color: Color(0xff13D18B), fontSize: 24),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              SizedBox(
                height: 120,
                width: double.infinity,
                child: Card(
                  elevation: 8,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text("Delivered Order",
                              style: TextStyle(
                                  color: Color(0xff0D83CB),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(
                              Icons.list,
                              size: 30.0,
                              color: Color(0xff13D18B),
                            ),
                            Text(
                              "Total: 0",
                              style: TextStyle(
                                  color: Color(0xff13D18B), fontSize: 24),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              SizedBox(
                height: 120,
                width: double.infinity,
                child: Card(
                  elevation: 8,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10.0),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text("Cancel Order",
                              style: TextStyle(
                                  color: Color(0xff0D83CB),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold)),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Icon(
                              Icons.list,
                              size: 30.0,
                              color: Color(0xff13D18B),
                            ),
                            Text(
                              "Total: 0",
                              style: TextStyle(
                                  color: Color(0xff13D18B), fontSize: 24),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
