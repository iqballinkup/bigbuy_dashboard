import 'package:big_buy_dashboard/const/const.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:jiffy/jiffy.dart';

class MyCoupon extends StatefulWidget {
  const MyCoupon({super.key});

  @override
  State<MyCoupon> createState() => _MyCouponState();
}

class _MyCouponState extends State<MyCoupon> {
  String? firstPickedDate;
  String? secondPickedDate;

  double textFontSize = 16.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: scaffoldColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 30,
                width: double.infinity,
                color: Color(0xff002A56),
                child: Center(
                  child: Text(
                    'Coupon List',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              SizedBox(
                height: 45,
                child: GestureDetector(
                  onTap: (() {
                    _selectedDate();
                  }),
                  child: TextFormField(
                    enabled: false,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: Icon(
                        Icons.calendar_month,
                        color: Colors.black87,
                      ),
                      border: OutlineInputBorder(borderSide: BorderSide.none),
                      hintText: firstPickedDate == null
                          ? Jiffy(DateTime.now()).format("dd - MMM - yyyy")
                          : firstPickedDate,
                      hintStyle: TextStyle(fontSize: 14, color: Colors.black87),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Center(
                child: Text(
                  "To",
                  style: TextStyle(
                      color: Colors.black54,
                      fontSize: textFontSize,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 45,
                child: GestureDetector(
                  onTap: (() {
                    _selectedDate2();
                  }),
                  child: TextFormField(
                    enabled: false,
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: Icon(
                        Icons.calendar_month,
                        color: Colors.black87,
                      ),
                      border: OutlineInputBorder(borderSide: BorderSide.none),
                      hintText: secondPickedDate == null
                          ? Jiffy(DateTime.now()).format("dd - MMM - yyyy")
                          : secondPickedDate,
                      hintStyle: TextStyle(fontSize: 14, color: Colors.black87),
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _selectedDate() async {
    final selectedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime(2050));
    if (selectedDate != null) {
      setState(() {
        firstPickedDate = Jiffy(selectedDate).format("dd - MMM - yyyy");
      });
    }
  }

  _selectedDate2() async {
    final selectedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1950),
        lastDate: DateTime(2050));
    if (selectedDate != null) {
      setState(() {
        secondPickedDate = Jiffy(selectedDate).format("dd - MMM - yyyy");
      });
    }
  }
}
