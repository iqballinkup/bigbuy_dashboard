import 'package:big_buy_dashboard/const/const.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Invite extends StatefulWidget {
  const Invite({super.key});

  @override
  State<Invite> createState() => _InviteState();
}

class _InviteState extends State<Invite> {
  TextEditingController inviteLinkController = TextEditingController();
  TextEditingController inviteSmsController = TextEditingController();
  TextEditingController inviteEmailController = TextEditingController();

  double textFormFieldHeight = 40.0;
  double textFontSize = 16.0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: scaffoldColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 30,
                width: double.infinity,
                color: Color(0xff002A56),
                child: Center(
                  child: Text(
                    'Invite',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                'Invitation Link',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: textFontSize,
                  color: Colors.black54,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: SizedBox(
                      height: textFormFieldHeight,
                      child: TextFormField(
                        controller: inviteLinkController,
                        enabled: false,
                        decoration: InputDecoration(
                          filled: true,
                          contentPadding: EdgeInsets.only(
                              left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                          fillColor: Color.fromARGB(255, 234, 231, 231),
                          border: OutlineInputBorder(),
                          hintText:
                              "https://bigbuy.com.bd/invitation/4965290897",
                          hintStyle: TextStyle(fontSize: 14),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return null;
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 3,
                  ),
                  Container(
                    height: 40,
                    color: Color.fromARGB(255, 218, 215, 215),
                    child: IconButton(
                      icon: const Icon(Icons.copy),
                      onPressed: () {
                        // ...
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                'Invite Via SMS',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: textFontSize,
                  color: Colors.black54,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: SizedBox(
                      height: textFormFieldHeight,
                      child: TextFormField(
                        controller: inviteSmsController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                          filled: true,
                          contentPadding: EdgeInsets.only(
                              left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                          fillColor: Colors.white,
                          border: OutlineInputBorder(),
                          hintText: "Enter Mobile Number",
                          hintStyle: TextStyle(fontSize: 14),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return null;
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 3,
                  ),
                  Container(
                    height: 40,
                    color: Color.fromARGB(255, 218, 215, 215),
                    child: IconButton(
                      icon: const Icon(Icons.send),
                      onPressed: () {
                        // ...
                      },
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 20.0,
              ),
              Text(
                'Invite Via EMail',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: textFontSize,
                  color: Colors.black54,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    child: SizedBox(
                      height: textFormFieldHeight,
                      child: TextFormField(
                        controller: inviteEmailController,
                        decoration: InputDecoration(
                          filled: true,
                          contentPadding: EdgeInsets.only(
                              left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                          fillColor: Colors.white,
                          border: OutlineInputBorder(),
                          hintText: "Enter Email",
                          hintStyle: TextStyle(fontSize: 14),
                        ),
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return null;
                          }
                          return null;
                        },
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 3,
                  ),
                  Container(
                    height: textFormFieldHeight,
                    color: Color.fromARGB(255, 218, 215, 215),
                    child: IconButton(
                      icon: const Icon(Icons.send),
                      onPressed: () {
                        // ...
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
