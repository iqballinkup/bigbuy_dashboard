import 'package:big_buy_dashboard/const/const.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ChangeMobile extends StatefulWidget {
  const ChangeMobile({super.key});

  @override
  State<ChangeMobile> createState() => _ChangeMobileState();
}

class _ChangeMobileState extends State<ChangeMobile> {
  double textFormFieldHeight = 40.0;
  double textFontSize = 16.0;

  TextEditingController mobileNoController = TextEditingController();
  TextEditingController newMobileNoController = TextEditingController();
  TextEditingController retypeMobileNoController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool _obscureText = true;

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: scaffoldColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 30,
                width: double.infinity,
                color: Color(0xff002A56),
                child: Center(
                  child: Text(
                    'Change Mobile No',
                    style: GoogleFonts.poppins(
                      textStyle: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Mobile No.',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: textFontSize,
                  color: Colors.black54,
                ),
              ),
              SizedBox(
                height: textFormFieldHeight,
                child: TextFormField(
                  controller: mobileNoController,
                  decoration: InputDecoration(
                    filled: true,
                    contentPadding: EdgeInsets.only(
                        left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                    fillColor: Colors.white,
                    border: OutlineInputBorder(),
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return null;
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'New Mobile No.',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: textFontSize,
                  color: Colors.black54,
                ),
              ),
              SizedBox(
                height: textFormFieldHeight,
                child: TextFormField(
                  controller: newMobileNoController,
                  decoration: InputDecoration(
                    filled: true,
                    contentPadding: EdgeInsets.only(
                        left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                    fillColor: Colors.white,
                    border: OutlineInputBorder(),
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return null;
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Re-type Mobile No.',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: textFontSize,
                  color: Colors.black54,
                ),
              ),
              SizedBox(
                height: textFormFieldHeight,
                child: TextFormField(
                  controller: retypeMobileNoController,
                  decoration: InputDecoration(
                    filled: true,
                    contentPadding: EdgeInsets.only(
                        left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                    fillColor: Colors.white,
                    border: OutlineInputBorder(),
                    hintStyle: TextStyle(fontSize: 14),
                  ),
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return null;
                    }
                    return null;
                  },
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Text(
                'Password',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: textFontSize,
                  color: Colors.black54,
                ),
              ),
              SizedBox(
                height: textFormFieldHeight,
                child: Align(
                  alignment: Alignment.center,
                  child: TextFormField(
                    controller: passwordController,
                    obscureText: _obscureText,
                    decoration: InputDecoration(
                        filled: true,
                        contentPadding: EdgeInsets.only(
                            left: 10.0, top: 0.0, bottom: 0.0, right: 0.0),
                        fillColor: Colors.white,
                        border: OutlineInputBorder(),
                        suffixIcon: IconButton(
                          icon: Icon(_obscureText
                              ? Icons.visibility_off
                              : Icons.visibility),
                          onPressed: () {
                            _toggle();
                          },
                        )),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return null;
                      }
                      return null;
                    },
                  ),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Align(
                  alignment: Alignment.centerRight,
                  child:
                      ElevatedButton(onPressed: () {}, child: Text("Update")))
            ],
          ),
        ),
      ),
    );
  }
}
